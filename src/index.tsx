import ReactDOM from "react-dom/client";

import "./index.css";

import { ColorChanger } from "./components/ColorChanger";
import { mockTasks, TasksComponent } from "./components/TaskList";
import reportWebVitals from "./reportWebVitals";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement,
);

root.render(
  <>
    <ColorChanger />
    <TasksComponent tasks={mockTasks} />
  </>,
);

reportWebVitals();
