/** @type {import('prettier').Options} */
module.exports = {
  useTabs: false,
  printWidth: 80,
  tabWidth: 2,
  singleQuote: false,
  trailingComma: 'all',

  overrides: [
    {
      files: ["*.ts", "*.js"],
      parser: 'typescript'
    },
    {
      files: ["*.json"],
      parser: 'json'
    }
  ]
};
